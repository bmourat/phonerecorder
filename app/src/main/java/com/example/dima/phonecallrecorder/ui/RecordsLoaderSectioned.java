package com.example.dima.phonecallrecorder.ui;

import android.content.Context;
import android.support.v4.content.AsyncTaskLoader;

import com.example.dima.phonecallrecorder.data.LoadRecordsImpl;
import com.example.dima.phonecallrecorder.data.RecordEntity;
import com.example.dima.phonecallrecorder.data.RecordsDbHelper;

import java.util.List;

/**
 * Created by dima on 10.03.16.
 */
public class RecordsLoaderSectioned extends AsyncTaskLoader<List<RecordsAdapter.ItemWrapper<?>>> {

    private RecordsDbHelper mDbHelper;
    private List<RecordsAdapter.ItemWrapper<?>> mList;
    private RecordEntity.STATUS[] mStatus;

    public RecordsLoaderSectioned(Context context, RecordEntity.STATUS... status) {
        super(context);
        mStatus = status;
        mDbHelper = new RecordsDbHelper(context);
    }

    @Override
    public List<RecordsAdapter.ItemWrapper<?>> loadInBackground() {
        LoadRecordsImpl loadRecords = new LoadRecordsImpl();
        return loadRecords.loadSectioned(mDbHelper, mStatus);
    }

    @Override
    protected void onStartLoading() {
        if (mList != null) {
            deliverResult(mList);
        }
        forceLoad();
    }

    @Override
    protected void onStopLoading() {
        cancelLoad();
    }

    @Override
    protected void onReset() {
        super.onReset();
        mList = null;
        onStopLoading();
    }

    @Override
    public void deliverResult(List<RecordsAdapter.ItemWrapper<?>> data) {
        mList = data;
        if (isStarted()) {
            super.deliverResult(data);
        }
    }
}
