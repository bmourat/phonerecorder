package com.example.dima.phonecallrecorder.ui;

import android.app.Activity;
import android.content.pm.PackageManager;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;

/**
 * Created by dima on 17.03.16.
 */
public class PermissionChecker {


    private final Activity mContext;

    public PermissionChecker(Activity context) {
        this.mContext = context;
    }

    public void check(String... perms) {
        for (String perm : perms) {
            if (ContextCompat.checkSelfPermission(mContext, perm) != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(mContext, new String[]{perm}, 11);
                return;
            }
        }
    }
}