package com.example.dima.phonecallrecorder.ui;

import com.example.dima.phonecallrecorder.data.RecordEntity;

import java.util.Set;

/**
 * Created by dima on 10.03.16.
 */
public interface OnSelectionChangeListener {
    void onSelectItems(Set<RecordEntity> selected);
}
