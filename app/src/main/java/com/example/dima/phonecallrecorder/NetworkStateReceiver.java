package com.example.dima.phonecallrecorder;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;

import com.example.dima.phonecallrecorder.ui.PrefsFragment;

public class NetworkStateReceiver extends BroadcastReceiver {

    private static final String TAG = "NetworkStateReceiver";

    public enum NetworkState {
        WIFI, MOBILE, NONE;
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        ConnectivityManager manager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo currentNetworkInfo = manager.getActiveNetworkInfo();
        if (currentNetworkInfo == null) {
            onNetworkStateChanged(NetworkState.NONE, context);
        } else if (currentNetworkInfo.getType() == ConnectivityManager.TYPE_WIFI) {
            onNetworkStateChanged(NetworkState.WIFI, context);
        } else {
            onNetworkStateChanged(NetworkState.MOBILE, context);
        }
    }

    protected  void onNetworkStateChanged(NetworkState s, Context context){
        if(s.equals(NetworkState.WIFI)){
            Log.d(TAG, "WIFI");
            forceSend(context);
        } else if(s.equals(NetworkState.MOBILE) && PrefsFragment.getKeyPrefConnectionType(context).equals(PrefsFragment.ConntectionType.WIFI_3G)){
            Log.d(TAG, "3G");
            forceSend(context);
        }
    }

    private void forceSend(Context context) {
        Intent intent = new Intent(context, SendFilesService.class);
        intent.setAction(SendFilesService.ACTION_SEND_PENDING_RECORDS);
        context.startService(intent);
    }
}