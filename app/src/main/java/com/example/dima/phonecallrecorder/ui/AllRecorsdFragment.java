package com.example.dima.phonecallrecorder.ui;


import android.app.Activity;
import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.example.dima.phonecallrecorder.HandleSavedRecordService;
import com.example.dima.phonecallrecorder.PlayerService;
import com.example.dima.phonecallrecorder.R;
import com.example.dima.phonecallrecorder.data.RecordEntity;

import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

/**
 * Created by dima on 03.03.16.
 */
public class AllRecorsdFragment extends Fragment implements LoaderManager.LoaderCallbacks<List<RecordsAdapter.ItemWrapper<?>>>,
        OnSelectionChangeListener {

    private static final String TAG = "AllRecorsdFragment";
    private static final int RECORDS_LOADER = 31;
    private static final int REQ_DELETE = 63;
    private static final int REQ_SEND = 62;
    public static final String SELECTED_RECORDS = "selectedRecords";
    private RecyclerView mRecyclerView;
    private RecordsAdapter mAdapter;
    private BroadcastReceiver mBroadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            reloadData();
        }
    };

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.records_fragment, container, false);

        mRecyclerView = (RecyclerView) view.findViewById(R.id.list);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this.getActivity(), LinearLayoutManager.VERTICAL, false));
        mAdapter = new RecordsAdapter(this);

        if (savedInstanceState != null) {
            HashSet<RecordEntity> recordEntities = (HashSet<RecordEntity>) savedInstanceState.getSerializable(SELECTED_RECORDS);
            mAdapter.setSelectedItems(recordEntities);
        }

        mRecyclerView.setAdapter(mAdapter);

        getLoaderManager().initLoader(RECORDS_LOADER, null, this);
        setHasOptionsMenu(true);
        return view;
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putSerializable(
                SELECTED_RECORDS, mAdapter.getSelectedItems());
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_main, menu);
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        if (getSelectedItemsCount() == 0) {
            menu.removeItem(R.id.action_play);
            menu.removeItem(R.id.action_delete);
            menu.removeItem(R.id.action_send);
        } else if (getSelectedItemsCount() == 1) {
            //all shown
        } else if (getSelectedItemsCount() > 0) {
            menu.removeItem(R.id.action_play);
        }
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        LocalBroadcastManager.getInstance(getContext()).registerReceiver(mBroadcastReceiver, new IntentFilter(HandleSavedRecordService.ACTION_RECORDS_UPDATED));
    }

    @Override
    public void onDetach() {
        super.onDetach();
        LocalBroadcastManager.getInstance(getContext()).unregisterReceiver(mBroadcastReceiver);
    }

    @Override
    public void onResume() {
        super.onResume();
        reloadData();
    }

    private void reloadData() {
        getLoaderManager().restartLoader(RECORDS_LOADER, null, this);
    }

    private int getSelectedItemsCount() {
        return mAdapter.getSelectedItems().size();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_play:
                Iterator<RecordEntity> iter = mAdapter.getSelectedItems().iterator();
                playSelectedRecord(iter.next());
                return true;
            case R.id.action_delete:
                deleteSelectedRecord();
                return true;
            case R.id.action_send:
                sendSelectedRecord();
                return true;
            case R.id.action_about:
                startActivity(new Intent(getContext(), AboutActivity.class));
                return true;
            case R.id.action_settings:
                startActivity(new Intent(getContext(), SettingsActivity.class));
                return true;
        }
        return super.onContextItemSelected(item);
    }



    private void playSelectedRecord(RecordEntity record) {
        Log.d(TAG, "playSelectedRecord " + record.getFilePath());
        Intent intent = new Intent(getContext(), PlayerService.class);
        intent.setAction(PlayerService.ACTION_PLAY);
        intent.putExtra(PlayerService.EXTRA_FILE, record.getFilePath());
        getContext().startService(intent);
    }

    private void stopPaly() {
        Intent intent = new Intent(getContext(), PlayerService.class);
        intent.setAction(PlayerService.ACTION_STOP);
        getContext().startService(intent);
    }

    private void sendSelectedRecord() {
        Log.d(TAG, "sendSelectedRecord count=");
        stopPaly();
        confirmSend();
    }

    private void confirmSend() {
        DialogFragment newFragment = ConfirmDialog.newInstance(
                R.string.action_send, getTag(), REQ_SEND);
        newFragment.show(getFragmentManager(), "dialog");
    }

    private void deleteSelectedRecord() {
        Log.d(TAG, "deleteSelectedRecord count=");
        stopPaly();
        confirmDelete();
    }

    @Override
    public android.support.v4.content.Loader<List<RecordsAdapter.ItemWrapper<?>>> onCreateLoader(int id, Bundle args) {
        if (id == RECORDS_LOADER) {
            return new RecordsLoaderSectioned(getActivity(), RecordEntity.STATUS.UNDEFINED, RecordEntity.STATUS.SEND_PENDING);
        }
        return null;
    }

    @Override
    public void onLoadFinished(android.support.v4.content.Loader<List<RecordsAdapter.ItemWrapper<?>>> loader, List<RecordsAdapter.ItemWrapper<?>> data) {
        mAdapter.setData(data);
    }

    @Override
    public void onLoaderReset(android.support.v4.content.Loader<List<RecordsAdapter.ItemWrapper<?>>> loader) {
        mAdapter.clear();
    }


    @Override
    public void onSelectItems(Set<RecordEntity> selected) {
        getActivity().invalidateOptionsMenu();
    }


    void confirmDelete() {
        DialogFragment newFragment = ConfirmDialog.newInstance(
                R.string.action_delete, getTag(), REQ_DELETE);
        newFragment.show(getFragmentManager(), "dialog");
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQ_DELETE && resultCode == Activity.RESULT_OK) {
            Log.d(TAG, "delete");
            deleteSelectedRecordImpl();
        } else if (requestCode == REQ_SEND && resultCode == Activity.RESULT_OK) {
            Log.d(TAG, "send");
            sendSelectedRecordImpl();
        }
    }

    private void sendSelectedRecordImpl() {
        Intent intent = new Intent(getContext(), HandleSavedRecordService.class);
        intent.setAction(HandleSavedRecordService.ACTION_FORCE_SEND_RECORDS);
        intent.putExtra(HandleSavedRecordService.EXTRA_RECORDS, mAdapter.getSelectedItems());
        getContext().startService(intent);
    }

    private void deleteSelectedRecordImpl() {
        Intent intent = new Intent(getContext(), HandleSavedRecordService.class);
        intent.setAction(HandleSavedRecordService.ACTION_DELETE_RECORDS);
        intent.putExtra(HandleSavedRecordService.EXTRA_RECORDS, mAdapter.getSelectedItems());
        getContext().startService(intent);
    }

    public static class ConfirmDialog extends DialogFragment {

        public static ConfirmDialog newInstance(int title, String tag, int reqCode) {
            ConfirmDialog frag = new ConfirmDialog();
            Bundle args = new Bundle();
            args.putInt("title", title);
            args.putString("tag", tag);
            args.putInt("reqCode", reqCode);
            frag.setArguments(args);
            return frag;
        }

        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            int title = getArguments().getInt("title");
            final String sourceFragmentTag = getArguments().getString("tag");
            final int reqCode = getArguments().getInt("reqCode");

            return new AlertDialog.Builder(getContext())
                    //.setIcon(R.drawable.alert_dialog_icon)
                    .setTitle(title)
                    .setPositiveButton(R.string.ok,
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int whichButton) {
                                    Fragment fragment = getActivity().getSupportFragmentManager().findFragmentByTag(sourceFragmentTag);
                                    if (fragment != null) {
                                        fragment.onActivityResult(reqCode, Activity.RESULT_OK, null);
                                    }
                                }
                            }
                    )
                    .setNegativeButton(R.string.cancel,
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int whichButton) {
                                    Fragment fragment = getActivity().getSupportFragmentManager().findFragmentByTag(sourceFragmentTag);
                                    if (fragment != null) {
                                        fragment.onActivityResult(reqCode, Activity.RESULT_CANCELED, null);
                                    }
                                }
                            }
                    )
                    .create();
        }


    }

}
