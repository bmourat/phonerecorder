package com.example.dima.phonecallrecorder.ui;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.dima.phonecallrecorder.HandleSavedRecordService;
import com.example.dima.phonecallrecorder.R;
import com.example.dima.phonecallrecorder.data.RecordEntity;

import java.text.SimpleDateFormat;
import java.util.List;

/**
 * Created by dima on 03.03.16.
 */
public class SendLogFragment extends Fragment implements LoaderManager.LoaderCallbacks<List<RecordEntity>> {

    private static final int RECORDS_LOADER = 52;
    private RecyclerView mRecyclerView;
    RecordsAdapter mAdapter;
    private BroadcastReceiver mBroadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            reloadData();
        }
    };


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.sendlog_fragment, container, false);

        mRecyclerView = (RecyclerView) view.findViewById(R.id.list);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this.getActivity(), LinearLayoutManager.VERTICAL, false));
        mAdapter = new RecordsAdapter();
        mRecyclerView.setAdapter(mAdapter);

        getLoaderManager().initLoader(RECORDS_LOADER, null, this);
        setHasOptionsMenu(true);
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        reloadData();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        LocalBroadcastManager.getInstance(getContext()).registerReceiver(mBroadcastReceiver, new IntentFilter(HandleSavedRecordService.ACTION_RECORDS_UPDATED));
    }

    @Override
    public void onDetach() {
        super.onDetach();
        LocalBroadcastManager.getInstance(getContext()).unregisterReceiver(mBroadcastReceiver);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_main, menu);
    }


    @Override
    public void onPrepareOptionsMenu(Menu menu) {
            menu.removeItem(R.id.action_play);
            menu.removeItem(R.id.action_delete);
            menu.removeItem(R.id.action_send);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_about:
                startActivity(new Intent(getContext(),AboutActivity.class));
                return true;
            case R.id.action_settings:
                startActivity(new Intent(getContext(), SettingsActivity.class));
                return true;
        }
        return super.onContextItemSelected(item);
    }

    private void reloadData() {
        getLoaderManager().restartLoader(RECORDS_LOADER, null, this);
    }

    @Override
    public android.support.v4.content.Loader<List<RecordEntity>> onCreateLoader(int id, Bundle args) {
        if (id == RECORDS_LOADER) {
            return new RecordsLoader(getActivity(), RecordEntity.STATUS.SEND_PENDING, RecordEntity.STATUS.SEND_OK);
        }
        return null;
    }

    @Override
    public void onLoadFinished(android.support.v4.content.Loader<List<RecordEntity>> loader, List<RecordEntity> data) {
        mAdapter.setData(data);
    }

    @Override
    public void onLoaderReset(android.support.v4.content.Loader<List<RecordEntity>> loader) {
        mAdapter.clear();
    }

    public static class RecordsAdapter extends RecyclerView.Adapter<RecordsAdapter.ViewHolder>  {

        private List<RecordEntity> mList;
        private SimpleDateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy HH:mm");

        @Override
        public RecordsAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

            View v = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.record_item, parent, false);
            ViewHolder vh = new ViewHolder((ViewGroup) v);
            return vh;
        }

        @Override
        public void onBindViewHolder(ViewHolder holder, int position) {
            holder.recordEntity = mList.get(position);
            holder.position = position;
            final long date = mList.get(position).getCallDate();
            holder.date.setText(dateFormat.format(date));
            holder.phone.setText(mList.get(position).getPhoneNumber());
            holder.contact.setText(mList.get(position).getContactName());

           if( mList.get(position).getStatus().equals(RecordEntity.STATUS.SEND_OK)){
               holder.check.setImageResource(android.R.drawable.presence_online);
           } else if(mList.get(position).getStatus().equals(RecordEntity.STATUS.SEND_PENDING)){
               holder.check.setImageResource(android.R.drawable.presence_invisible);
           }
        }

        @Override
        public int getItemCount() {
            return mList == null ? 0 : mList.size();
        }

        public void setData(List<RecordEntity> data) {
            mList = data;
            notifyDataSetChanged();
        }

        public void clear() {
            mList = null;
            notifyDataSetChanged();
        }

        public class ViewHolder extends RecyclerView.ViewHolder {
            private TextView date;
            private TextView phone;
            private TextView contact;
            private ImageView check;
            private RecordEntity recordEntity;
            private View background;
            private int position;

            public ViewHolder(View itemView) {
                super(itemView);
                date = (TextView) itemView.findViewById(R.id.date);
                phone = (TextView) itemView.findViewById(R.id.phone);
                contact = (TextView) itemView.findViewById(R.id.contact);
                check = (ImageView) itemView.findViewById(R.id.check);
                background = itemView;
            }
        }
    }


}
