package com.example.dima.phonecallrecorder.ui;

import android.content.Context;
import android.support.v4.content.AsyncTaskLoader;

import com.example.dima.phonecallrecorder.data.LoadRecordsImpl;
import com.example.dima.phonecallrecorder.data.RecordEntity;
import com.example.dima.phonecallrecorder.data.RecordsDbHelper;

import java.util.List;

/**
 * Created by dima on 10.03.16.
 */
public class RecordsLoader extends AsyncTaskLoader<List<RecordEntity>> {

    private RecordsDbHelper mDbHelper;
    private List<RecordEntity> mList;
    private RecordEntity.STATUS[] mStatus;

    public RecordsLoader(Context context, RecordEntity.STATUS... status) {
        super(context);
        mStatus = status;
        mDbHelper = new RecordsDbHelper(context);
    }

    @Override
    public List<RecordEntity> loadInBackground() {
        LoadRecordsImpl loadRecords = new LoadRecordsImpl();
        return loadRecords.load(mDbHelper, mStatus);
    }

    @Override
    protected void onStartLoading() {
        if (mList != null) {
            deliverResult(mList);
        }
        forceLoad();
    }

    @Override
    protected void onStopLoading() {
        cancelLoad();
    }

    @Override
    protected void onReset() {
        super.onReset();
        mList = null;
        onStopLoading();
    }

    @Override
    public void deliverResult(List<RecordEntity> data) {
        mList = data;
        if (isStarted()) {
            super.deliverResult(data);
        }
    }
}
