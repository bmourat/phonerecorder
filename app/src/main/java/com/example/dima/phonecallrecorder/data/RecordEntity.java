package com.example.dima.phonecallrecorder.data;

import android.provider.BaseColumns;

import java.io.Serializable;

/**
 * Created by dima on 09.03.16.
 */
public class RecordEntity implements BaseColumns , Serializable {
    public static final String TABLE_NAME = "phone_records";

    private final long id;

    private final String mPhoneNumber;
    public static final String COLUMN_PHONE_NUMBER = "phone_number";

    private final String mFilePath;
    public static final String COLUMN_FILE_PATH = "file_path";

    private final long callDate;
    public static final String COLUMN_DATE = "call_date";

    private final STATUS mStatus;
    public static final  String COLUMN_STATUS = "status";

    private final String mName;
    public static final  String COLUMN_CONTACT_NAME = "contact_name";

    private static final String TEXT_TYPE = " TEXT";
    private static final String TEXT_DATE = " INTEGER";
    private static final String COMMA_SEP = ",";

    public static final String SQL_CREATE_ENTRIES =
            "CREATE TABLE " + RecordEntity.TABLE_NAME + " (" +
                    RecordEntity._ID + " INTEGER PRIMARY KEY," +
                    RecordEntity.COLUMN_PHONE_NUMBER + TEXT_TYPE + COMMA_SEP +
                    RecordEntity.COLUMN_DATE + TEXT_DATE + COMMA_SEP +
                    RecordEntity.COLUMN_FILE_PATH + TEXT_TYPE + COMMA_SEP +
                    RecordEntity.COLUMN_STATUS + TEXT_TYPE  + COMMA_SEP +
                    RecordEntity.COLUMN_CONTACT_NAME + TEXT_TYPE +
    " )";

    public static final String SQL_DELETE_ENTRIES =
            "DROP TABLE IF EXISTS " + RecordEntity.TABLE_NAME;

    public RecordEntity(long id, String mPhoneNumber, String mFilePath, long callDate, STATUS stats) {
        this(id, mPhoneNumber, mFilePath, callDate, stats, null);
    }

    public RecordEntity(long id, String mPhoneNumber, String mFilePath, long callDate, STATUS stats, String name) {
        this.id = id;
        this.mPhoneNumber = mPhoneNumber;
        this.mFilePath = mFilePath;
        this.callDate = callDate;
        this.mStatus = stats;
        this.mName = name;
    }


    public long getId() {
        return id;
    }

    public long getCallDate() {
        return callDate;
    }


    public String getPhoneNumber() {
        return mPhoneNumber;
    }

    public String getFilePath() {
        return mFilePath;
    }

    public STATUS getStatus() {
        return mStatus;
    }

    public String getContactName() {
        return mName;
    }

    public enum STATUS {
        UNDEFINED,
        SEND_PENDING,
        SEND_OK
    }
}
