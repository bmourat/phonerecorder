package com.example.dima.phonecallrecorder;

import android.app.Application;

import com.example.dima.phonecallrecorder.ui.PrefsFragment;
import com.example.dima.phonecallrecorder.util.LogManager;
import com.facebook.stetho.Stetho;

/**
 * Created by SBT-Baysangurov-MT on 06.06.2016.
 */
public class App extends Application {

	private static LogManager logManager;

	@Override
	public void onCreate() {
		super.onCreate();
		logManager = LogManager.getLogger(this, PrefsFragment.getPrefManagerId(this));
		Stetho.initializeWithDefaults(this);
	}

	public static LogManager getLogger(){
		return logManager;
	}
}
