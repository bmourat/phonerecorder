package com.example.dima.phonecallrecorder;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import android.util.Log;

/**
 * Created by dima on 03.03.16.
 */
public class PhoneStateReceiver extends BroadcastReceiver {
    public static final String TAG = "PhoneStateReceiver";
    public static final String OFFHOOK = "OFFHOOK";
    public static final String IDLE = "IDLE";

    @Override
    public void onReceive(Context context, Intent intent) {
        Log.d(TAG, "acton = " + intent.getAction());
        final String state = intent.getStringExtra(TelephonyManager.EXTRA_STATE);
        Log.d(TAG, "state = " + String.valueOf(state));
        String number1 = intent.getStringExtra(TelephonyManager.EXTRA_INCOMING_NUMBER);
        Log.d(TAG, "EXTRA_INCOMING_NUMBER = " + String.valueOf(number1));
        String number2 = intent.getStringExtra(Intent.EXTRA_PHONE_NUMBER);
        Log.d(TAG, "EXTRA_PHONE_NUMBER = " + String.valueOf(number2));

        if (IDLE.equals(state)) {
            stopRecording(context);
        }

        if (OFFHOOK.equals(state)) {
            String number = intent.getStringExtra(TelephonyManager.EXTRA_INCOMING_NUMBER);
            if (!TextUtils.isEmpty(number)) {
                startRecording(context, number);
            }
        }

        if (intent.getAction().equals(Intent.ACTION_NEW_OUTGOING_CALL)) {
            String number = intent.getStringExtra(Intent.EXTRA_PHONE_NUMBER);
            startRecording(context, number);
        }

    }

    private void stopRecording(Context context) {
        Log.d(TAG, "stopRecording");
        Intent intent = new Intent(context, RecorderService.class);
        intent.setAction(RecorderService.ACTION_STOP_RECORD);
        context.startService(intent);
    }

    private void startRecording(Context context, String number) {
        Log.d(TAG, "startRecording number= " + number);
        Intent intent = new Intent(context, RecorderService.class);
        intent.setAction(RecorderService.ACTION_START_RECORD);
        intent.putExtra(RecorderService.EXTRA_PHONE_NUMBER, number);
        context.startService(intent);
    }
}

