package com.example.dima.phonecallrecorder;

import android.app.IntentService;
import android.content.ContentValues;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.support.v4.content.LocalBroadcastManager;
import android.text.TextUtils;
import android.util.Log;

import com.example.dima.phonecallrecorder.data.RecordEntity;
import com.example.dima.phonecallrecorder.data.RecordsDbHelper;
import com.example.dima.phonecallrecorder.ui.PrefsFragment;

import java.io.File;
import java.util.Date;
import java.util.HashSet;

/**
 * Created by dima on 09.03.16.
 */
public class HandleSavedRecordService extends IntentService {

	public static final String ACTION_FORCE_SEND_RECORDS = "com.example.dima.phonecallrecorder.HandleSavedRecordService.ACTION_FORCE_SEND_RECORDS";
	public static final String ACTION_MARK_RECORD_SEND_OK = "com.example.dima.phonecallrecorder.HandleSavedRecordService.ACTION_MARK_RECORD_SEND_OK";
	public static final String ACTION_DELETE_RECORDS = "com.example.dima.phonecallrecorder.HandleSavedRecordService.ACTION_DELETE_RECORDS";
	public static final String EXTRA_RECORDS = "com.example.dima.phonecallrecorder.HandleSavedRecordService.EXTRA_RECORDS";


	public static final String ACTION_NEW_RECORD = "com.example.dima.phonecallrecorder.HandleSavedRecordService.action_handle";
	public static final String EXTRA_NUMBER = "com.example.dima.phonecallrecorder.HandleSavedRecordService.EXTRA_NUMBER";
	public static final String EXTRA_DATE = "com.example.dima.phonecallrecorder.HandleSavedRecordService.EXTRA_DATE";
	public static final String EXTRA_FILE = "com.example.dima.phonecallrecorder.HandleSavedRecordService.EXTRA_FILE";
	public static final String EXTRA_RECORD = "com.example.dima.phonecallrecorder.HandleSavedRecordService.EXTRA_RECORD";
	private static final String TAG = "HandleRecordService";
	public static final String ACTION_RECORDS_UPDATED = "ACTION_RECORDS_UPDATED";

	private RecordsDbHelper mRecordsDbHelper;

	public HandleSavedRecordService(String name) {
		this();
	}

	public HandleSavedRecordService() {
		super("HandleSavedRecordService");
	}

	@Override
	protected void onHandleIntent(Intent intent) {
		if (ACTION_NEW_RECORD.equals(intent.getAction())) {
			if (handleNewRecord(intent).equals(RecordEntity.STATUS.SEND_PENDING)) {
				forceSend();
			}
		} else if (ACTION_FORCE_SEND_RECORDS.equals(intent.getAction())) {
			markRecordsPendingForSend(intent);
			forceSend();
		} else if (ACTION_DELETE_RECORDS.equals(intent.getAction())) {
			deleteRecords(intent);
		} else if (ACTION_MARK_RECORD_SEND_OK.equals(intent.getAction())) {
			markRecordAsSendOk(intent);
		}
	}

	private void markRecordAsSendOk(Intent intent) {
		RecordEntity entity = (RecordEntity) intent.getSerializableExtra(EXTRA_RECORD);

		File file = new File(entity.getFilePath());
		boolean b = file.delete();
		Log.d(TAG, "delete file  " + b);

		SQLiteDatabase db = mRecordsDbHelper.getWritableDatabase();
		ContentValues values = new ContentValues();
		values.put(RecordEntity.COLUMN_STATUS, RecordEntity.STATUS.SEND_OK.name());
		int i = db.update(RecordEntity.TABLE_NAME, values, RecordEntity._ID + " = " + entity.getId() + " ;", null);
		Log.d(TAG, "update count = " + i + " records");
		db.close();
		sendNotification();
	}


	@Override
	public void onCreate() {
		super.onCreate();
		mRecordsDbHelper = new RecordsDbHelper(this);
	}

	private void deleteRecords(Intent intent) {
		HashSet<RecordEntity> recordEntities = (HashSet<RecordEntity>) intent.getSerializableExtra(EXTRA_RECORDS);

		int cout = 0;
		for (RecordEntity record : recordEntities) {
			File file = new File(record.getFilePath());
			boolean b = file.delete();
			cout += b ? 1 : 0;
		}
		Log.d(TAG, "delete count = " + cout + " files");


		StringBuilder sb = new StringBuilder();
		for (RecordEntity record : recordEntities) {
			sb.append(record.getId()).append(", ");
		}
		sb.setLength(sb.length() - 2);
		SQLiteDatabase db = mRecordsDbHelper.getWritableDatabase();
		int i = db.delete(RecordEntity.TABLE_NAME, RecordEntity._ID + " IN ( " + sb.toString() + " );", null);
		Log.d(TAG, "delete count = " + i + " records");
		db.close();
		sendNotification();
	}

	private void markRecordsPendingForSend(Intent intent) {
		HashSet<RecordEntity> recordEntities = (HashSet<RecordEntity>) intent.getSerializableExtra(EXTRA_RECORDS);
		StringBuilder sb = new StringBuilder();
		for (RecordEntity record : recordEntities) {
			sb.append(record.getId()).append(", ");
		}
		sb.setLength(sb.length() - 2);
		SQLiteDatabase db = mRecordsDbHelper.getWritableDatabase();
		ContentValues values = new ContentValues();
		values.put(RecordEntity.COLUMN_STATUS, RecordEntity.STATUS.SEND_PENDING.name());
		int i = db.update(RecordEntity.TABLE_NAME, values, RecordEntity._ID + " IN ( " + sb.toString() + " );", null);
		Log.d(TAG, "update count = " + i + " records");
		db.close();
		sendNotification();
	}

	private RecordEntity.STATUS handleNewRecord(Intent intent) {
		String file = intent.getStringExtra(EXTRA_FILE);
		String phoneNumber = intent.getStringExtra(EXTRA_NUMBER);
		long date = intent.getLongExtra(EXTRA_DATE, new Date().getTime());
		FindContact findContact = new FindContact(getApplicationContext());
		String name = findContact.getContactDisplayNameByNumber(phoneNumber);
		Log.d(TAG, "findContact name = " + String.valueOf(name));
		RecordEntity.STATUS status = defineStatus(name);
		insertRecord(phoneNumber, date, file, status, name);
		return status;
	}

	private RecordEntity.STATUS defineStatus(String contactName) {
		String contactMask = PrefsFragment.getPrefContactMask(getApplicationContext());
		Log.d(TAG, "findContact contactMask = " + String.valueOf(contactMask));
		if (contactMask.trim().equals("*") || contactMask.trim().isEmpty())
			return RecordEntity.STATUS.SEND_PENDING;

		if (!TextUtils.isEmpty(contactName)) {
			if (contactName.toLowerCase().contains(contactMask.toLowerCase())) {
				return RecordEntity.STATUS.SEND_PENDING;
			}
		}
		return RecordEntity.STATUS.UNDEFINED;
	}

	private void forceSend() {
		Intent intent = new Intent(this, SendFilesService.class);
		intent.setAction(SendFilesService.ACTION_SEND_PENDING_RECORDS);
		startService(intent);
	}

	private void insertRecord(String phoneNumber, long date, String file, RecordEntity.STATUS status, String name) {
		SQLiteDatabase db = mRecordsDbHelper.getWritableDatabase();

		ContentValues values = new ContentValues();
		values.put(RecordEntity.COLUMN_PHONE_NUMBER, phoneNumber);
		values.put(RecordEntity.COLUMN_DATE, date);
		values.put(RecordEntity.COLUMN_FILE_PATH, file);
		values.put(RecordEntity.COLUMN_STATUS, status.name());
		if (!TextUtils.isEmpty(name)) {
			values.put(RecordEntity.COLUMN_CONTACT_NAME, name);
		}

		long newRowId = db.insert(
				RecordEntity.TABLE_NAME,
				null,
				values);
		db.close();
		Log.d(TAG, "insertRecord newRowId=" + newRowId);
		sendNotification();
	}


	private void sendNotification() {
		Intent intent = new Intent(ACTION_RECORDS_UPDATED);
		LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
	}
}
