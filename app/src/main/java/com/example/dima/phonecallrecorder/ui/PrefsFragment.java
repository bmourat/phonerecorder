package com.example.dima.phonecallrecorder.ui;

import android.content.Context;
import android.media.MediaRecorder;
import android.os.Bundle;
import android.preference.Preference;
import android.preference.PreferenceFragment;
import android.preference.PreferenceManager;

import com.example.dima.phonecallrecorder.R;

/**
 * Created by dima on 16.03.16.
 */
public class PrefsFragment extends PreferenceFragment {

    public static final String KEY_PREF_MANAGER_ID = "pref_manager_id";
    public static final String KEY_PREF_CONTACT_MASK = "pref_contact_mask";
    public static final String KEY_PREF_SERVER_URL = "pref_server_url";
    public static final String KEY_PREF_CONNECTION_TYPE = "pref_connection_type";
    public static final String KEY_PREF_AUDIO_SRC = "pref_audio_src";

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        addPreferencesFromResource(R.xml.preferences);
        setListener();
    }

    private void setListener() {

        findPreference(KEY_PREF_AUDIO_SRC).setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
            @Override
            public boolean onPreferenceChange(Preference preference, Object newValue) {
                preference.setSummary(String.valueOf(newValue));
                return true;
            }
        });


        findPreference(KEY_PREF_MANAGER_ID).setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
            @Override
            public boolean onPreferenceChange(Preference preference, Object newValue) {
                preference.setSummary(String.valueOf(newValue));
                return true;
            }
        });

        findPreference(KEY_PREF_CONTACT_MASK).setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
            @Override
            public boolean onPreferenceChange(Preference preference, Object newValue) {
                preference.setSummary(String.valueOf(newValue));
                return true;
            }
        });

        findPreference(KEY_PREF_SERVER_URL).setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
            @Override
            public boolean onPreferenceChange(Preference preference, Object newValue) {
                preference.setSummary(String.valueOf(newValue));
                return true;
            }
        });


        findPreference(KEY_PREF_CONNECTION_TYPE).setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
            @Override
            public boolean onPreferenceChange(Preference preference, Object newValue) {
                preference.setSummary(ConntectionType.valueOf(String.valueOf(newValue)).str(getActivity()));
                return true;
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        updateSummary();
    }

    public static String getPrefManagerId(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context).getString(KEY_PREF_MANAGER_ID, context.getString(R.string.default_manager_id));
    }

    public static String getPrefContactMask(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context).getString(KEY_PREF_CONTACT_MASK, "");
    }

    public static String getPrefServerUrl(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context).getString(KEY_PREF_SERVER_URL, context.getString(R.string.default_server));
    }

    public static ConntectionType getKeyPrefConnectionType(Context context) {
        String val = PreferenceManager.getDefaultSharedPreferences(context).getString(KEY_PREF_CONNECTION_TYPE, "WIFI");
        return ConntectionType.valueOf(val);
    }

    public static AudioSouce getKeyPrefAudioSrc(Context context) {
        String val = PreferenceManager.getDefaultSharedPreferences(context).getString(KEY_PREF_AUDIO_SRC, "VOICE_CALL");
        return AudioSouce.valueOf(val);
    }


    private void updateSummary() {
        updateManager();
        updateMask();
        updateServer();
        updateConnetcion();
        updateAudioSrc();
    }

    private void updateAudioSrc() {
        Preference preference = findPreference(KEY_PREF_AUDIO_SRC);
        preference.setSummary(getKeyPrefAudioSrc(getActivity()).name());
    }

    private void updateConnetcion() {
        Preference preference;
        preference = findPreference(KEY_PREF_CONNECTION_TYPE);
        preference.setSummary(getKeyPrefConnectionType(getActivity()).str(getActivity()));
    }

    private void updateServer() {
        Preference preference;
        preference = findPreference(KEY_PREF_SERVER_URL);
        preference.setSummary(getPrefServerUrl(getActivity()));
    }

    private void updateMask() {
        Preference preference;
        preference = findPreference(KEY_PREF_CONTACT_MASK);
        preference.setSummary(getPrefContactMask(getActivity()));
    }

    private void updateManager() {
        Preference preference = findPreference(KEY_PREF_MANAGER_ID);
        preference.setSummary(getPrefManagerId(getActivity()));
    }

    public enum ConntectionType {
        WIFI(R.string.wifi),
        WIFI_3G(R.string.wifi3g);

        ConntectionType(int mStrId) {
            this.mStrId = mStrId;
        }

        private final int mStrId;

        public String str(Context c) {
            return c.getResources().getString(mStrId);
        }
    }

    public enum AudioSouce {
        MIC(MediaRecorder.AudioSource.MIC),
        VOICE_CALL(MediaRecorder.AudioSource.VOICE_CALL),
        VOICE_COMMUNICATION(MediaRecorder.AudioSource.VOICE_COMMUNICATION),
        VOICE_RECOGNITION(MediaRecorder.AudioSource.VOICE_RECOGNITION);

        private final int src;

        AudioSouce(int src) {
            this.src = src;
        }

        public int getSrc() {
            return src;
        }
    }

}