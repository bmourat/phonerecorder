package com.example.dima.phonecallrecorder.data;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.example.dima.phonecallrecorder.data.RecordEntity;
import com.example.dima.phonecallrecorder.data.RecordsDbHelper;
import com.example.dima.phonecallrecorder.ui.RecordsAdapter;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by dima on 15.03.16.
 */
public class LoadRecordsImpl {

    private SimpleDateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy");

    public List<RecordEntity> load(RecordsDbHelper mDbHelper ,RecordEntity.STATUS ... mStatus) {
        SQLiteDatabase db = mDbHelper.getReadableDatabase();
        String[] projection = {
                RecordEntity._ID,
                RecordEntity.COLUMN_DATE,
                RecordEntity.COLUMN_PHONE_NUMBER,
                RecordEntity.COLUMN_FILE_PATH,
                RecordEntity.COLUMN_STATUS,
                RecordEntity.COLUMN_CONTACT_NAME
        };

        StringBuilder sb = new StringBuilder();
        for(RecordEntity.STATUS s:mStatus){
            sb.append("'").append(s.name()).append("', ");
        }
        sb.setLength(sb.length() - 2);

        String sortOrder = RecordEntity.COLUMN_DATE + " DESC";
        Cursor c = db.query(
                RecordEntity.TABLE_NAME,  // The table to query
                projection,                               // The columns to return
                RecordEntity.COLUMN_STATUS + " IN ( " + sb.toString() + " ) ",   // The columns for the WHERE clause
                null,                            // The values for the WHERE clause
                null,                                     // don't group the rows
                null,                                     // don't filter by row groups
                sortOrder                                 // The sort order
        );

        List<RecordEntity> list = new ArrayList<>();
        if (c != null) {
            c.moveToFirst();
            while (!c.isAfterLast()) {
                list.add(buildRecord(c));
                c.moveToNext();
            }
            c.close();
        }
        db.close();
        return list;
    }

    public List<RecordsAdapter.ItemWrapper<?>> loadSectioned(RecordsDbHelper mDbHelper ,RecordEntity.STATUS ... mStatus) {
        List<RecordEntity> list = load(mDbHelper, mStatus);
        List<RecordsAdapter.ItemWrapper<?>> sectioned = new ArrayList<>();
        String date = null;
        for(RecordEntity entity: list){
           String dateNew = dateFormat.format(entity.getCallDate());
            if(!dateNew.equals(date)){
                sectioned.add(new RecordsAdapter.SectionWrapper(dateNew));
            }
            date = dateNew;
            sectioned.add(new RecordsAdapter.RecordWrapper(entity));
        }
        return sectioned;
    }



    private RecordEntity buildRecord(Cursor c) {
        long id = c.getLong(c.getColumnIndex(RecordEntity._ID));
        String phone = c.getString(c.getColumnIndex(RecordEntity.COLUMN_PHONE_NUMBER));
        String path = c.getString(c.getColumnIndex(RecordEntity.COLUMN_FILE_PATH));
        long date = c.getLong(c.getColumnIndex(RecordEntity.COLUMN_DATE));
        RecordEntity.STATUS status = RecordEntity.STATUS.valueOf(c.getString(c.getColumnIndex(RecordEntity.COLUMN_STATUS)));
        String name = c.getString(c.getColumnIndex(RecordEntity.COLUMN_CONTACT_NAME));
        return new RecordEntity(id, phone, path, date, status, name);
    }

}
