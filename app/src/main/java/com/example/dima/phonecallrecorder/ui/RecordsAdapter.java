package com.example.dima.phonecallrecorder.ui;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.dima.phonecallrecorder.R;
import com.example.dima.phonecallrecorder.data.RecordEntity;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Created by dima on 17.03.16.
 */
public class RecordsAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> implements ItemClickListener {

	private static final int TYPE_RECORD = 1;
	private static final int TYPE_SECTION = 2;
	private List<ItemWrapper<?>> mList;
	private HashSet<RecordEntity> mSelectedItems = new HashSet<>();
	private final OnSelectionChangeListener mOnSelectionChangeListener;
	private SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm");

	public RecordsAdapter(OnSelectionChangeListener selectionChangeListener) {
		this.mOnSelectionChangeListener = selectionChangeListener;
	}


	@Override
	public void onItemClick(int position) {
		RecordEntity recordEntity = (RecordEntity) mList.get(position).getItem();
		if (mSelectedItems.contains(recordEntity)) {
			mSelectedItems.remove(recordEntity);
		} else {
			mSelectedItems.add(recordEntity);
		}
		notifyItemChanged(position);
		if (mOnSelectionChangeListener != null) {
			mOnSelectionChangeListener.onSelectItems(mSelectedItems);
		}
	}

	@Override
	public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
		if (viewType == TYPE_RECORD) {
			View v = LayoutInflater.from(parent.getContext())
							 .inflate(R.layout.record_item, parent, false);
			return new RecordViewHolder(v, this);
		} else if (viewType == TYPE_SECTION) {
			View v = LayoutInflater.from(parent.getContext())
							 .inflate(R.layout.section_item, parent, false);
			return new SectionViewHolder(v);
		}
		return null;
	}

	@Override
	public int getItemViewType(int position) {
		return mList.get(position).getViewType();
	}

	private boolean isItemSelected(int position) {
		return mSelectedItems.contains(mList.get(position).getItem());
	}

	public HashSet<RecordEntity> getSelectedItems() {
		return mSelectedItems;
	}

	public void setSelectedItems(HashSet<RecordEntity> mSelectedItems) {
		this.mSelectedItems = mSelectedItems;
	}

	@Override
	public void onBindViewHolder(RecyclerView.ViewHolder h, int position) {
		final int type = getItemViewType(position);
		if (type == TYPE_RECORD) {
			RecordViewHolder holder = (RecordViewHolder) h;
			RecordEntity item = (RecordEntity) mList.get(position).getItem();
			holder.recordEntity = item;
			holder.position = position;
			final long date = item.getCallDate();
			holder.date.setText(dateFormat.format(date));
			holder.phone.setText(item.getPhoneNumber());
			holder.contact.setText(item.getContactName());

			holder.check.setImageResource(item.getStatus() == RecordEntity.STATUS.UNDEFINED ?
												  android.R.drawable.presence_offline :
												  android.R.drawable.presence_online);

			//holder.check.setVisibility(isItemSelected(position) ? View.VISIBLE : View.INVISIBLE);
			holder.background.setBackgroundResource(isItemSelected(position) ? R.color.colorSelectedRow : R.color.colorNormalRow);
		} else if (type == TYPE_SECTION) {
			SectionViewHolder holder = (SectionViewHolder) h;
			String datestr = (String) mList.get(position).getItem();
			holder.date.setText(datestr);
		}
	}

	@Override
	public int getItemCount() {
		return mList == null ? 0 : mList.size();
	}

	public void setData(List<RecordsAdapter.ItemWrapper<?>> data) {
		mList = data;
		updateSelection();
		notifyDataSetChanged();
	}

	private void updateSelection() {
		Iterator<RecordEntity> iterator = mSelectedItems.iterator();

		Set<Object> set = new HashSet<>();
		for (ItemWrapper<?> item : mList) {
			set.add(item.getItem());
		}

		while (iterator.hasNext()) {
			RecordEntity entity = iterator.next();
			if (!set.contains(entity)) {
				iterator.remove();
			}
		}
	}

	public void clear() {
		mList = null;
		mSelectedItems.clear();
		notifyDataSetChanged();
	}

	public static class RecordViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
		private TextView date;
		private TextView phone;
		private TextView contact;
		private ImageView check;
		private RecordEntity recordEntity;
		private ItemClickListener mListener;
		private View background;
		private int position;

		public RecordViewHolder(View itemView, ItemClickListener listener) {
			super(itemView);
			mListener = listener;
			date = (TextView) itemView.findViewById(R.id.date);
			phone = (TextView) itemView.findViewById(R.id.phone);
			contact = (TextView) itemView.findViewById(R.id.contact);
			check = (ImageView) itemView.findViewById(R.id.check);
			background = itemView;
			background.setOnClickListener(this);
		}

		@Override
		public void onClick(View v) {
			mListener.onItemClick(position);
		}
	}


	public static class SectionViewHolder extends RecyclerView.ViewHolder {
		private TextView date;

		public SectionViewHolder(View itemView) {
			super(itemView);
			date = (TextView) itemView.findViewById(R.id.date);
		}
	}


	public abstract static class ItemWrapper<T> {
		private final T item;
		private final int viewType;

		protected ItemWrapper(T item, int viewType) {
			this.item = item;
			this.viewType = viewType;
		}

		public T getItem() {
			return item;
		}

		public int getViewType() {
			return viewType;
		}
	}

	public static class RecordWrapper extends ItemWrapper<RecordEntity> {
		public RecordWrapper(RecordEntity item) {
			super(item, TYPE_RECORD);
		}
	}

	public static class SectionWrapper extends ItemWrapper<String> {
		public SectionWrapper(String item) {
			super(item, TYPE_SECTION);
		}
	}

}
