package com.example.dima.phonecallrecorder.ui;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.widget.TextView;

import com.example.dima.phonecallrecorder.BuildConfig;
import com.example.dima.phonecallrecorder.R;

/**
 * Created by dima on 03.03.16.
 */
public class AboutActivity extends AppCompatActivity {
    private Toolbar mToolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about);

        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);
        ((TextView)findViewById(R.id.version)).setText("version " + String.valueOf(BuildConfig.VERSION_NAME));
    }
}
