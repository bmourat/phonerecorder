package com.example.dima.phonecallrecorder;

import android.app.IntentService;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;

import com.example.dima.phonecallrecorder.data.LoadRecordsImpl;
import com.example.dima.phonecallrecorder.data.RecordEntity;
import com.example.dima.phonecallrecorder.data.RecordsDbHelper;
import com.example.dima.phonecallrecorder.ui.PrefsFragment;

import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.Collection;
import java.util.List;

/**
 * Created by dima on 15.03.16.
 */
public class SendFilesService extends IntentService {


	public static final String ACTION_SEND_PENDING_RECORDS = "com.example.dima.phonecallrecorder.SendFilesService.ACTION_SEND_PENDING_RECORDS";
	private static final String TAG = "SendFilesService";
	private RecordsDbHelper mDbHelper;

	public SendFilesService() {
		super("SendFilesService");
	}

	public SendFilesService(String name) {
		super(name);
	}

	@Override
	public void onCreate() {
		super.onCreate();
		mDbHelper = new RecordsDbHelper(this);
	}

	@Override
	protected void onHandleIntent(Intent intent) {
		if (ACTION_SEND_PENDING_RECORDS.equals(intent.getAction())) {
			try {
				if (isValidNetworkState())
					sendRecords(this);
			} catch (IOException e) {
				Log.e(TAG, e.getMessage());
			}
		}
	}

	private void sendRecords(Context context) throws IOException {
		LoadRecordsImpl loadRecords = new LoadRecordsImpl();
		List<RecordEntity> recordEntities = loadRecords.load(mDbHelper, RecordEntity.STATUS.SEND_PENDING);

		App.getLogger().log(TAG, "Number of files to send: " + recordEntities.size());

		if (recordEntities.isEmpty()) {
			Log.d(TAG, "recordEntities.isEmpty");
			return;
		}

		SftpSender sender = new SftpSender();
		byte[] buffer = getPrivateKeyBytes(context);
		App.getLogger().log(TAG, "Connecting to server");
		if (sender.connect("ftpuser", PrefsFragment.getPrefServerUrl(context), 22, buffer)) {

			sendRecords(recordEntities, sender);

			if(sender.changeDirectory("logs"))
				sendLogFiles(sender);

			sender.close();
		} else {
			Log.e(TAG, "Server connection failed");
		}
	}

	private void sendRecords(List<RecordEntity> recordEntities, SftpSender sender) {
		for (RecordEntity entity : recordEntities) {
			App.getLogger().log(TAG, "Sending file: " + entity.getFilePath());
			if (sender.send(entity.getFilePath())) {
				markRecordAsSendOK(entity);
				App.getLogger().log(TAG, "File send successfully");
			} else {
				// todo error show
				break;
			}
		}
	}

	private void sendLogFiles(SftpSender sender) {
		String logFilesDirectory = getExternalFilesDir(null).getAbsolutePath() + File.separator + "logs";
		Collection<File> logFiles = FileUtils.listFiles(new File(logFilesDirectory), new String[]{"log"}, false);

		for(File log : logFiles){
			App.getLogger().log(TAG, "Sending file: " + log.getPath());

			if (sender.send(log.getPath())) {
				FileUtils.deleteQuietly(log);
				App.getLogger().log(TAG, "File send successfully");
			} else {
				// todo error show
				break;
			}
		}
	}

	private byte[] getPrivateKeyBytes(Context context) throws IOException {
		InputStream is = context.getAssets().open("ftpuser.pem");
		int size = is.available();
		byte[] buffer = new byte[size]; //declare the size of the byte array with size of the file
		is.read(buffer); //read file
		is.close(); //close file
		return buffer;
	}

	private void markRecordAsSendOK(RecordEntity entity) {
		Intent intent = new Intent(this, HandleSavedRecordService.class);
		intent.setAction(HandleSavedRecordService.ACTION_MARK_RECORD_SEND_OK);
		intent.putExtra(HandleSavedRecordService.EXTRA_RECORD, entity);
		this.startService(intent);
	}

	public boolean isValidNetworkState() {
		ConnectivityManager manager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo currentNetworkInfo = manager.getActiveNetworkInfo();
		if(currentNetworkInfo == null) {

			App.getLogger().log(TAG, "Network not found");

			return false;
		}

		if(PrefsFragment.getKeyPrefConnectionType(this) == PrefsFragment.ConntectionType.WIFI &&
		   currentNetworkInfo.getType() == ConnectivityManager.TYPE_MOBILE) {

			App.getLogger().log(TAG, "Network type doesn't match");

			return false;
		}

		return true;
	}
}
