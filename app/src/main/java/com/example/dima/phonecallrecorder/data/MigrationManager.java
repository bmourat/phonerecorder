package com.example.dima.phonecallrecorder.data;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Environment;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.filefilter.FileFilterUtils;

import java.io.File;
import java.io.IOException;
import java.util.Collection;

/**
 * Created by SBT-Baysangurov-MT on 09.06.2016.
 */
public class MigrationManager {

	public static void migrate(Context context, RecordsDbHelper dbHelper){

		SQLiteDatabase db = dbHelper.getWritableDatabase();
		copyPendingFiles(context);
		updateAllPendingAndUndefinedRecords(context, db);
		db.close();
	}

	private static void updateAllPendingAndUndefinedRecords(Context context, SQLiteDatabase db) {
		String selection = RecordEntity.COLUMN_STATUS + " IN (?, ?);";
		String selectionArgs[] = new String[]{RecordEntity.STATUS.SEND_PENDING.name(),RecordEntity.STATUS.UNDEFINED.name()};
		Cursor cursor = db.query(RecordEntity.TABLE_NAME, null, selection, selectionArgs, null, null, null);

		String newDirectory = context.getExternalFilesDir(null).getAbsolutePath();
		while (cursor.moveToNext()){

			String filePath = cursor.getString(cursor.getColumnIndex(RecordEntity.COLUMN_FILE_PATH));
			ContentValues values = new ContentValues();
			values.put(RecordEntity.COLUMN_FILE_PATH, generateNewFilePath(newDirectory, filePath));
			values.put(RecordEntity.COLUMN_STATUS, RecordEntity.STATUS.SEND_PENDING.name());

			db.update(RecordEntity.TABLE_NAME, values, RecordEntity._ID + " = " + cursor.getInt(0), null);
		}
		cursor.close();
	}

	private static String generateNewFilePath(String newDirectory, String oldFilePath) {
		int startIndex = oldFilePath.lastIndexOf(File.separatorChar);
		return newDirectory + oldFilePath.substring(startIndex);
	}

	private static void copyPendingFiles(Context context){

		File sourceDirectory = new File(Environment.getExternalStorageDirectory().getAbsolutePath());
		File destDirectory = new File(context.getExternalFilesDir(null).getAbsolutePath());

		try {
			Collection<File> recordFiles = FileUtils.listFiles(sourceDirectory, new String[]{"3gp"}, false);
			FileUtils.copyDirectory(sourceDirectory, destDirectory, FileFilterUtils.suffixFileFilter(".3gp"));
			for (File file : recordFiles) {
				FileUtils.deleteQuietly(file);
			}

		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static boolean isUpgradeNeeded() {
		File sourceDirectory = new File(Environment.getExternalStorageDirectory().getAbsolutePath());
		return FileUtils.listFiles(sourceDirectory, new String[]{"3gp"}, false).size() > 0;
	}

}
