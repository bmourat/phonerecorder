package com.example.dima.phonecallrecorder.ui;

import android.content.Intent;
import android.media.MediaPlayer;
import android.media.MediaRecorder;
import android.os.Bundle;
import android.os.Environment;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.RadioGroup;

import com.example.dima.phonecallrecorder.R;
import com.example.dima.phonecallrecorder.RecorderService;

import java.io.IOException;

public class MainActivity extends AppCompatActivity {

    private static final String LOG_TAG = "AudioRecordTest";
    private int mAudioSrc;

    private MediaPlayer mPlayer = null;
    private Button mRecordButton;
    private Button mPlayButton;
    State state = State.IDLE;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("voice recording test app");
        setSupportActionBar(toolbar);

        RadioGroup radioGroup = (RadioGroup) findViewById(R.id.source);
        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                onSourceSelected(checkedId);
            }
        });
        radioGroup.check(R.id.MIC);

        mRecordButton = (Button) findViewById(R.id.record_btn);
        mPlayButton = (Button) findViewById(R.id.play_btn);

        mRecordButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onRecord();
            }
        });

        mPlayButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onPlay();
            }
        });

        updateControls();
    }

    private void updateControls() {
        switch (state){
            case IDLE:
                mRecordButton.setEnabled(true);
                mRecordButton.setText("Start Record");
                mPlayButton.setEnabled(true);
                mPlayButton.setText("Start Play");
                break;

            case RECORD:
                mRecordButton.setEnabled(true);
                mRecordButton.setText("Stop");
                mPlayButton.setEnabled(false);
                mPlayButton.setText("Start Play");
                break;

            case PLAYED:
                mRecordButton.setEnabled(false);
                mRecordButton.setText("Start Record");
                mPlayButton.setEnabled(true);
                mPlayButton.setText("Stop");
                break;

        }
    }

    private void onRecord() {
        if(state == State.RECORD){
            state = State.IDLE;
            stopRecording();
            updateControls();
        } else if(state == State.IDLE){
            state = State.RECORD;
            startRecording();
            updateControls();
        }
    }

    private void onPlay() {
        if(state == State.PLAYED){
            state = State.IDLE;
            stopPlaying();
            updateControls();
        } else if(state == State.IDLE){
            state = State.PLAYED;
            startPlaying();
            updateControls();
        }
    }

    private void startPlaying() {
      /*
       mPlayer = new MediaPlayer();
        try {
            mPlayer.setDataSource(RecorderService.crateFileName());
            mPlayer.prepare();
            mPlayer.start();
            mPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                @Override
                public void onCompletion(MediaPlayer mp) {
                    state = State.IDLE;
                    updateControls();
                }
            });
        } catch (IOException e) {
            Log.e(LOG_TAG, "prepare() failed");
        }*/
    }

    private void stopPlaying() {
        mPlayer.release();
        mPlayer = null;
    }

    private void startRecording() {
        Intent intent = new Intent(this, RecorderService.class);
        intent.setAction(RecorderService.ACTION_START_RECORD);
        //intent.putExtra(RecorderService.EXTRA_SRC, getAudioSrc());
        //intent.putExtra(RecorderService.EXTRA_FILE, mFileName);
        startService(intent);
    }

    private void stopRecording() {
        Intent intent = new Intent(this, RecorderService.class);
        intent.setAction(RecorderService.ACTION_STOP_RECORD);
        startService(intent);
    }
    private void onSourceSelected(int checkedId) {
        switch (checkedId){
            case R.id.MIC:
                setSource(MediaRecorder.AudioSource.MIC);
                break;
            case R.id.VOICE_CALL:
                setSource(MediaRecorder.AudioSource.VOICE_CALL);
                break;
            case R.id.VOICE_COMMUNICATION:
                setSource(MediaRecorder.AudioSource.VOICE_COMMUNICATION);
                break;
            case R.id.VOICE_RECOGNITION:
                setSource(MediaRecorder.AudioSource.VOICE_RECOGNITION);
                break;
            case R.id.VOICE_UPLINK:
                setSource(MediaRecorder.AudioSource.VOICE_UPLINK);
                break;
            case R.id.VOICE_DOWNLINK:
                setSource(MediaRecorder.AudioSource.VOICE_DOWNLINK);
                break;
            default:
                setSource(MediaRecorder.AudioSource.DEFAULT);
        }
    }

    private void setSource(int source) {
        mAudioSrc = source;
    }

    public int getAudioSrc() {
        return mAudioSrc;
    }

    @Override
    public void onPause() {
        super.onPause();
        if (mPlayer != null) {
            mPlayer.release();
            mPlayer = null;
        }
    }

    enum State {
        IDLE,        RECORD,         PLAYED;
    }

}
