package com.example.dima.phonecallrecorder.ui;

import android.Manifest;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;

import com.example.dima.phonecallrecorder.R;
import com.example.dima.phonecallrecorder.data.MigrationManager;
import com.example.dima.phonecallrecorder.data.RecordsDbHelper;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by dima on 03.03.16.
 */
public class RecorsdActivity extends AppCompatActivity {

	private static final String TAG = "RecorsdActivity";

	private Toolbar mToolbar;
	private TabLayout mTabLayout;
	private ViewPager mViewPager;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_records);

		if (MigrationManager.isUpgradeNeeded()) {
			Log.d(TAG, "Migration needed");
			MigrationManager.migrate(this, new RecordsDbHelper(this));
		}

		mToolbar = (Toolbar) findViewById(R.id.toolbar);
		setSupportActionBar(mToolbar);

		mViewPager = (ViewPager) findViewById(R.id.viewpager);
		setupViewPager(mViewPager);
		mTabLayout = (TabLayout) findViewById(R.id.tabs);
		mTabLayout.setupWithViewPager(mViewPager);

		checkPermission();
	}

	@Override
	public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
		checkPermission();
	}

	private void checkPermission() {
		PermissionChecker checker = new PermissionChecker(this);
		checker.check(
				Manifest.permission.READ_CONTACTS,
				Manifest.permission.RECORD_AUDIO,
				Manifest.permission.WRITE_EXTERNAL_STORAGE,
				Manifest.permission.READ_PHONE_STATE,
				Manifest.permission.INTERNET,
				Manifest.permission.ACCESS_NETWORK_STATE
		);
	}

	private void setupViewPager(ViewPager viewPager) {
		ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
		adapter.addFragment(new AllRecorsdFragment(), getString(R.string.fragment_records));
		adapter.addFragment(new SendLogFragment(), getString(R.string.fragment_log));
		viewPager.setAdapter(adapter);
	}

	class ViewPagerAdapter extends FragmentPagerAdapter {
		private final List<Fragment> mFragmentList = new ArrayList<>();
		private final List<String> mFragmentTitleList = new ArrayList<>();

		public ViewPagerAdapter(FragmentManager manager) {
			super(manager);
		}

		@Override
		public Fragment getItem(int position) {
			return mFragmentList.get(position);
		}

		@Override
		public int getCount() {
			return mFragmentList.size();
		}

		public void addFragment(Fragment fragment, String title) {
			mFragmentList.add(fragment);
			mFragmentTitleList.add(title);
		}

		@Override
		public CharSequence getPageTitle(int position) {
			return mFragmentTitleList.get(position);
		}
	}


}
