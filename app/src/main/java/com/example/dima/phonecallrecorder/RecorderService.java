package com.example.dima.phonecallrecorder;

import android.app.NotificationManager;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.media.MediaRecorder;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.support.v4.app.NotificationCompat;
import android.util.Log;
import android.widget.Toast;

import com.example.dima.phonecallrecorder.ui.PrefsFragment;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by dima on 28.01.16.
 */
public class RecorderService extends Service {

	private static String TAG = "RecorderService";
	private final int mNotificationId = 34643;
	private String mFile;
	private long mCallDate;
	private String mNumber;

	public static final String ACTION_START_RECORD = "com.example.dima.phonecallrecorder.RecorderService.ACTION_START_RECORD";
	public static final String ACTION_STOP_RECORD = "com.example.dima.phonecallrecorder.RecorderService.ACTION_STOP_RECORD";
	public static final String EXTRA_PHONE_NUMBER = "EXTRA_PHONE_NUMBER";

	private MediaRecorder mRecorder = null;

	@Nullable
	@Override
	public IBinder onBind(Intent intent) {
		return null;
	}

	@Override
	public void onCreate() {
		super.onCreate();
	}

	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		if (intent != null && ACTION_START_RECORD.equals(intent.getAction())) {
			if (mRecorder == null) {
				mNumber = intent.getStringExtra(RecorderService.EXTRA_PHONE_NUMBER);
				mCallDate = new Date().getTime();
				Log.d(TAG, "onStartCommand number = " + mNumber);
				mFile = crateFileName();
				startRecording();
			}
		} else if (intent != null && (ACTION_STOP_RECORD.equals(intent.getAction()))) {
			if (mRecorder != null) {
				stopRecording();
			}
		}
		return super.onStartCommand(intent, flags, startId);
	}

	public String crateFileName() {
		//TODO direction
		String direction = "I";
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd-HH-mm");
		String dateStr = dateFormat.format(mCallDate);
		StringBuilder sb = new StringBuilder();
		sb.append(getRecordsDirectoryPath()).append("/").append(dateStr).append("-")
			.append(getManagerId()).append("-").append(mNumber).append(direction).append(".3gp");
		return sb.toString();
	}

	String getRecordsDirectoryPath() {
		return getExternalFilesDir(null).getAbsolutePath();
		//return Environment.getExternalStorageDirectory().getAbsolutePath();
	}

	private String getManagerId() {
		return PrefsFragment.getPrefManagerId(getApplication());
	}

	public int getAudioSource() {
		return PrefsFragment.getKeyPrefAudioSrc(getApplicationContext()).getSrc();
	}

	private void startRecording() {
		Log.d(TAG, "startRecording to file = " + mFile);
		mRecorder = new MediaRecorder();
		Log.d(TAG, "startRecording audio src = " + getAudioSource());
		mRecorder.setAudioSource(getAudioSource());
		//todo format ?
		mRecorder.setOutputFormat(MediaRecorder.OutputFormat.THREE_GPP);
		mRecorder.setOutputFile(mFile);
		mRecorder.setAudioEncoder(MediaRecorder.AudioEncoder.AMR_NB);

		try {
			mRecorder.prepare();
		} catch (IOException e) {
			Log.e("service", "prepare() failed");
		}
		try {
			mRecorder.start();
		} catch (RuntimeException e) {
			mRecorder.release();
			mRecorder = null;
			if (e.getMessage().contains("start failed")) {
				Log.e(TAG, e.getMessage());
				Toast.makeText(this, "please select other Audio Source", Toast.LENGTH_LONG).show();
			} else {
				throw e;
			}
		}

		Log.d(TAG, "stoptRecording out");
		showNotification();
	}

	private void stopRecording() {
		Log.d(TAG, "stoptRecording file " + mFile);
		if (mRecorder != null) {
			mRecorder.stop();
			mRecorder.release();
			mRecorder = null;
		}
		hideNotification();
		handleSavedRecord();
		stopSelf();
	}

	private void handleSavedRecord() {

		App.getLogger().log(TAG, "File " + mFile + " recorded");

		Intent intent = new Intent(this, HandleSavedRecordService.class);
		intent.setAction(HandleSavedRecordService.ACTION_NEW_RECORD);
		intent.putExtra(HandleSavedRecordService.EXTRA_NUMBER, mNumber);
		intent.putExtra(HandleSavedRecordService.EXTRA_DATE, mCallDate);
		intent.putExtra(HandleSavedRecordService.EXTRA_FILE, mFile);
		startService(intent);
	}

	private void showNotification() {
		NotificationCompat.Builder mBuilder =
				new NotificationCompat.Builder(this)
						.setSmallIcon(R.drawable.ic_sync_black_24dp)
						.setContentTitle("Phone call saved")
						.setContentText("Phone call saved");
		NotificationManager notificationManager =
				(NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
		notificationManager.notify(mNotificationId, mBuilder.build());
	}

	private void hideNotification() {
		NotificationManager notificationManager =
				(NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
		notificationManager.cancel(mNotificationId);
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
	}
}
