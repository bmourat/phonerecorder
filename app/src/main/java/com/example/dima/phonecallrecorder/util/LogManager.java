package com.example.dima.phonecallrecorder.util;

import android.content.Context;
import android.util.Log;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.time.DateFormatUtils;
import org.apache.commons.lang3.time.DateUtils;

import java.io.File;
import java.io.IOException;
import java.util.Date;

/**
 * Created by SBT-Baysangurov-MT on 22.06.2016.
 */
public class LogManager {

	private static final String TAG = "LogManager";

	private static LogManager INSTANCE;

	private static final String logFileName = "application.today";
	private static final String logFileExtension = ".log";

	private Date currentDate;
	private FileLogger fileLogger;
	private String managerId;
	private String logFilesDirectory;
	private String fullLogFileName;


	public static LogManager getLogger(Context context, String managerId){
		if(INSTANCE == null)
			INSTANCE = new LogManager(context, managerId);

		return INSTANCE;
	}

	public void log(String tag, String message){
		//if(new Date().getMinutes() - currentDate.getMinutes() < 5)
		if(DateUtils.isSameDay(currentDate, new Date()))
			fileLogger.log(tag, message);
		else{
			performDateChange();
			log(tag, message);
		}
	}

	public void setManagerId(String managerId) {
		this.managerId = managerId;
	}

	private LogManager(Context context, String managerId){
		this.managerId = managerId;
		logFilesDirectory = context.getExternalFilesDir(null).getAbsolutePath() +
						 File.separator + "logs";

		createLogFilesDirectory();

		fullLogFileName = logFilesDirectory + File.separator + logFileName;
		fileLogger = FileLogger.getLogger(fullLogFileName);
		currentDate = new Date();
	}

	private void createLogFilesDirectory() {
		try {
			FileUtils.forceMkdir(new File(logFilesDirectory));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private void performDateChange(){
		try {
			FileUtils.moveFile(new File(fullLogFileName), new File(getYesterdayLogFileName()));
			fileLogger = FileLogger.getLogger(fullLogFileName);
			currentDate = new Date();
		} catch (IOException e) {
			Log.e(TAG, "Renaming log file failed",  e);
		}

	}

	private String getYesterdayLogFileName(){
		return
			new StringBuilder()
				.append(logFilesDirectory)
				.append(File.separator)
				.append(DateFormatUtils.format(currentDate, "yyyy_MM_dd_ss_"))
				.append(managerId)
				.append(logFileExtension)
				.toString();
	}
}
