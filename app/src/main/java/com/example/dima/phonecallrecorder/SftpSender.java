package com.example.dima.phonecallrecorder;

import android.util.Log;

import com.jcraft.jsch.Channel;
import com.jcraft.jsch.ChannelSftp;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.Session;
import com.jcraft.jsch.SftpException;
import com.jcraft.jsch.SftpProgressMonitor;

import java.io.BufferedReader;
import java.io.File;
import java.io.InputStreamReader;
import java.util.Vector;

/**
 * Created by dima on 18.03.16.
 */
public class SftpSender {

	private static final String TAG = "SftpSender";

	private ChannelSftp mChannel;
	private Session mSession;


	public boolean connect(String user, String host, int port, byte[] prvkey) {
		try {
			JSch jsch = new JSch();
			jsch.addIdentity("name", prvkey, null, null);
			Log.d(TAG, "addIdentity ");
			return openSession(user, host, port, jsch);

		} catch (JSchException e) {
			App.getLogger().log(TAG, e.getMessage());
			Log.e(TAG, e.getMessage());
		}
		return false;
	}

	private boolean openSession(String user, String host, int port, JSch jsch) throws JSchException {
		mSession = jsch.getSession(user, host, port);
		mSession.setConfig("StrictHostKeyChecking", "no");
		mSession.connect();
		Log.d(TAG, "session.connect()");
		Channel channel = mSession.openChannel("sftp");
		channel.connect();
		Log.d(TAG, "channel.connect()");
		mChannel = (ChannelSftp) channel;
		return true;
	}

	public boolean connect(String user, String host, int port, String privateKey) {
		try {
			JSch jsch = new JSch();
			jsch.addIdentity(privateKey);
			Log.d(TAG, "addIdentity " + privateKey);
			return openSession(user, host, port, jsch);

		} catch (JSchException e) {
			Log.e(TAG, e.getMessage());
		}
		return false;
	}

	public Vector ls() throws SftpException {
		return mChannel.ls(".");
	}

	boolean send(String locatFilePath) {
		Log.d(TAG, "send file = " + locatFilePath);
		String file = new File(locatFilePath).getName();
		SftpProgressMonitor monitor = new MyProgressMonitor();
		try {
			mChannel.put(locatFilePath, file, monitor, ChannelSftp.OVERWRITE);
		} catch (SftpException e) {
			App.getLogger().log(TAG, e.getMessage());
			Log.e(TAG, e.getMessage());
			return false;
		}
		Log.d(TAG, "send file OK ");
		return true;
	}


	boolean changeDirectory(String directory){
		try {
			mChannel.cd(directory);
		} catch (SftpException e) {

			if(ChannelSftp.SSH_FX_FAILURE == e.id){
				if(createDirectory(directory)){
					try {
						mChannel.cd(directory);
					}
					catch (SftpException ex){
						App.getLogger().log(TAG, ex.getMessage());
						Log.e(TAG, e.getMessage());
						return false;
					}
				}
			}
			else {
				App.getLogger().log(TAG, e.getMessage());
				Log.e(TAG, e.getMessage());
				return false;
			}

		}

		Log.d(TAG, "Change directory OK ");
		return true;
	}

	boolean createDirectory(String directory){
		try {
			mChannel.mkdir(directory);
		} catch (SftpException e) {
			App.getLogger().log(TAG, e.getMessage());
			Log.e(TAG, e.getMessage());
			return false;
		}
		return true;
	}

	public void close() {
		if (mChannel.isConnected()) {
			mChannel.disconnect();
			mChannel = null;
		}
		if (mSession.isConnected()) {
			mSession.disconnect();
			mSession = null;
		}
	}


	private class MyProgressMonitor implements SftpProgressMonitor {
		@Override
		public void init(int op, String src, String dest, long max) {
			Log.d(TAG, "init");
		}

		@Override
		public boolean count(long count) {
			Log.d(TAG, "cont=" + count);
			return false;
		}

		@Override
		public void end() {
			Log.d(TAG, "end");
		}
	}
}
