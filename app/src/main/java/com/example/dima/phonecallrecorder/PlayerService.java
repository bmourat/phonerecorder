package com.example.dima.phonecallrecorder;

import android.app.Notification;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import java.io.File;
import java.io.FileDescriptor;
import java.io.FileInputStream;
import java.io.IOException;

/**
 * Created by dima on 11.03.16.
 */
public class PlayerService extends Service implements MediaPlayer.OnPreparedListener {
    public static final String ACTION_PLAY = "com.example.dima.phonecallrecorder.PlayerService.action.PLAY";
    public static final String ACTION_STOP = "com.example.dima.phonecallrecorder.PlayerService.action.STOP";
    private static final String TAG = "PlayerService";
    private static final int NOTIFICATION_ID = 42;
    public static String EXTRA_FILE = "EXTRA_FILE";

    private MediaPlayer mMediaPlayer = null;
    private String mFile;

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    private void stopPlay() {
        if (mMediaPlayer != null) {
            mMediaPlayer.release();
            mMediaPlayer = null;
        }
        stopForeground(true);
    }

    public int onStartCommand(Intent intent, int flags, int startId) {
        if (intent.getAction() != null && intent.getAction().equals(ACTION_PLAY)) {
            stopPlay();
            mFile = intent.getStringExtra(EXTRA_FILE);
            mMediaPlayer = new MediaPlayer();
            Log.d(TAG, "onStartCommand ACTION_PLAY file= " + mFile);
            try {
                 mMediaPlayer.setDataSource(mFile);
                //FileInputStream inputStream = new FileInputStream(mFile);
                //mMediaPlayer.setDataSource(inputStream.getFD());
            } catch (IOException e) {
                Log.e(TAG, "onStartCommand", e);
                stopPlay();
                stopSelf();
                return super.onStartCommand(intent, flags, startId);
            }
            mMediaPlayer.setOnPreparedListener(this);
            mMediaPlayer.prepareAsync(); // prepare async to not block main thread
        }
        if (intent.getAction() != null  && intent.getAction().equals(ACTION_STOP)) {
            Log.d(TAG, "onStartCommand ACTION_STOP");
            stopPlay();
            stopSelf();
        }

        return super.onStartCommand(intent, flags, startId);
    }

    public void onPrepared(MediaPlayer player) {
        Log.d(TAG, "onPrepared");
        player.start();
        player.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {

            @Override
            public void onCompletion(MediaPlayer mp) {
                Log.d(TAG, "onCompletion");
                stopPlay();
                stopSelf();
            }
        });

        startForeground(NOTIFICATION_ID, buildNotification());
    }

    private Notification buildNotification() {
        Intent intent = new Intent(getApplicationContext(), PlayerService.class);
        intent.setAction(PlayerService.ACTION_STOP);

        PendingIntent pi = PendingIntent.getService(getApplicationContext(), 0,
                intent,
                PendingIntent.FLAG_UPDATE_CURRENT);

        NotificationCompat.Builder builder =
                new NotificationCompat.Builder(this)
                        .setContentIntent(pi)
                        .setOngoing(true)
                        .setSmallIcon(android.R.drawable.ic_media_play)
                        .setContentTitle("Click to stop")
                        .setContentText( "Play:" + new File(mFile).getName());
        return  builder.build();
    }

    //TODO impl
    private void updatePlayProgress ( ){
        mMediaPlayer.getDuration();
        mMediaPlayer.getCurrentPosition();
    }

}

