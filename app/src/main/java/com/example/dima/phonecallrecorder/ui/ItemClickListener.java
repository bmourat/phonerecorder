package com.example.dima.phonecallrecorder.ui;

/**
 * Created by dima on 17.03.16.
 */
interface ItemClickListener {
    void onItemClick(int position);
}
