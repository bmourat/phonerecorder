package com.example.dima.phonecallrecorder;

import android.test.AndroidTestCase;

import com.jcraft.jsch.SftpException;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Vector;

/**
 * Created by dima on 18.03.16.
 */
public class SftpSenderTest extends AndroidTestCase{

    public void testSftp() throws SftpException, IOException {
        SftpSender sender = new SftpSender();

        InputStream is = getContext().getAssets().open("Mobile_Dictaphone_ftpuser.pem");
        int size = is.available();
        byte[] buffer = new byte[size]; //declare the size of the byte array with size of the file
        is.read(buffer); //read file
        is.close(); //close file


        assertTrue(sender.connect("ftpuser", "52.18.20.214", 22, buffer));

        //assertTrue(sender.connect("ftpuser", "52.18.20.214", 22, "/sdcard/ftpuser.pem"));


        Vector v = sender.ls();
        assertFalse(v.isEmpty());
    }
}
